<?php

use Laravel\Lumen\Routing\Router;

return static function (Router $router): void {
    $router->group(['prefix' => '/admin'], static function () use ($router) {
        // Users CRUD
        $router->get('/users', 'UserController@all');
        $router->get('/users/{id}', 'UserController@find');
        $router->post('/users', 'UserController@store');
        $router->put('/users/{id}', 'UserController@update');
        $router->delete('/users/{id}', 'UserController@delete');

        // UserWallets CRUD
        $router->get('/users/{user_id}/wallets', 'UserWalletController@all');
        $router->get('/users/{user_id}/wallets/{id}', 'UserWalletController@find');
        $router->post('/users/{user_id}/wallets', 'UserWalletController@store');
        $router->put('/users/{user_id}/wallets/{id}', 'UserWalletController@update');
        $router->delete('/users/{user_id}/wallets/{id}', 'UserWalletController@delete');

        // ATMs CRUD
        $router->get('/atms', 'ATMController@all');
        $router->get('/atms/{id}', 'ATMController@find');
        $router->post('/atms', 'ATMController@store');
        $router->put('/atms/{id}', 'ATMController@update');
        $router->delete('/atms/{id}', 'ATMController@delete');

        // ATMBankNotes CRUD
        $router->get('/atms/{atm_id}/bank_notes', 'ATMBankNoteController@all');
        $router->get('/atms/{atm_id}/bank_notes/{id}', 'ATMBankNoteController@find');
        $router->post('/atms/{atm_id}/bank_notes', 'ATMBankNoteController@store');
        $router->put('/atms/{atm_id}/bank_notes/{id}', 'ATMBankNoteController@update');
        $router->delete('/atms/{atm_id}/bank_notes/{id}', 'ATMBankNoteController@delete');
    });
};
