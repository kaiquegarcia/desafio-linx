<?php

use Laravel\Lumen\Routing\Router;

return static function (Router $router): void
{
    $router->get('/', fn() => $router->app->version());

    $router->group(['prefix' => '/api'], static function () use ($router) {
        $adminRoutes = require_once __DIR__ . "/admin.php";
        $userRoutes = require_once __DIR__ . "/user.php";

        $adminRoutes($router);
        $userRoutes($router);
    });
};
