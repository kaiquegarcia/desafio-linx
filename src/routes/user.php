<?php

use Laravel\Lumen\Routing\Router;

return static function (Router $router): void {
    // Ações de carteiras (contas)
    $router->group(['middleware' => \App\Http\Middleware\UserAuth::class], function() use ($router) {
        $router->get('/wallets', 'UserWalletController@allFromUser');
        $router->get('/wallets/{id}', 'UserWalletController@findFromUser');
        $router->get('/wallets/{id}/movements', 'UserWalletController@allMovements'); // Extrato
        $router->patch('/wallets/{id}/deposit', 'UserWalletController@deposit'); // Depósito
        $router->patch('/wallets/{id}/withdraw', 'UserWalletController@withdraw'); // Saque
        $router->post('/wallets/{id}/transfer', ''); // Transferência
    });
};
