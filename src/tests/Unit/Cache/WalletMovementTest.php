<?php

namespace Tests\Unit\Cache;

use App\Cache\WalletMovement;
use App\Enums\OperationType;
use Spatie\Enum\Laravel\Faker\FakerEnumProvider;
use Tests\TestCase;

class WalletMovementTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->faker->addProvider(new FakerEnumProvider($this->faker));
    }

    public function operationTypeDataProvider(): array
    {
        return array_map(
            fn(OperationType $operationType) => [$operationType],
            OperationType::cases()
        );
    }

    /**
     * @test
     */
    public function shouldSetIdAndCreatedAtOnPrePersist(): void
    {
        $movement = new WalletMovement();
        self::assertNull($movement->getId());
        self::assertNull($movement->getCreatedAt());
        $movement->prePersist();
        self::assertNotNull($movement->getId());
        self::assertNotNull($movement->getCreatedAt());
    }

    private function getMovementInstanceAndAssertions(array $attributes = []): array
    {
        $id = $attributes['id']
            ?? $this->faker->boolean() ? $this->faker->uuid() : null;
        $wallet_id = $attributes['wallet_id']
            ?? $this->faker->uuid();
        $atm_id = $attributes['atm_id']
            ?? $this->faker->boolean() ? $this->faker->uuid() : null;
        $operation_type = $attributes['operation_type']
            ?? $this->faker->randomEnum(OperationType::class);
        $amount = $attributes['amount']
            ?? $this->faker->randomNumber();
        $created_at = $attributes['created_at']
            ?? $id ? $this->faker->date() : null;

        return [
            new WalletMovement(
                id: $id,
                wallet_id: $wallet_id,
                atm_id: $atm_id,
                operation_type: $operation_type,
                amount: $amount,
                created_at: $created_at
            ),
            [
                'id' => $id,
                'wallet_id' => $wallet_id,
                'atm_id' => $atm_id,
                'operation_type' => $operation_type,
                'amount' => $amount,
                'created_at' => $created_at,
            ]
        ];
    }

    /**
     * @test
     * @dataProvider operationTypeDataProvider
     */
    public function shouldInstanceWithData(OperationType $operationType): void
    {
        /** @var WalletMovement $movement */
        [$movement, $assert] = $this->getMovementInstanceAndAssertions(['operation_type' => $operationType]);
        self::assertEquals($assert['id'], $movement->getId());
        self::assertEquals($assert['wallet_id'], $movement->getWalletId());
        self::assertEquals($assert['atm_id'], $movement->getAtmId());
        self::assertEquals($operationType, $movement->getOperationType());
        self::assertEquals($assert['amount'], $movement->getAmount());
        self::assertEquals($assert['created_at'], $movement->getCreatedAt());
    }

    /**
     * @test
     */
    public function shouldSerializeByJsonEncode(): void
    {
        [$movement, $assert] = $this->getMovementInstanceAndAssertions();
        $stringFromArray = json_encode($assert);
        $stringFromInstance = json_encode($movement);
        self::assertEquals($stringFromArray, $stringFromInstance);
    }

    /**
     * @test
     */
    public function shouldUnserializeFromJsonDecode(): void
    {
        [$movement, $assert] = $this->getMovementInstanceAndAssertions();
        $encode = json_encode($movement);
        $decode = WalletMovement::jsonUnserialize($encode);
        self::assertEquals($movement, $decode);
    }
}
