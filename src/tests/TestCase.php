<?php

namespace Tests;

use Faker\Factory;
use Faker\Generator as Faker;
use Laravel\Lumen\Application;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Mockery;
use Mockery\MockInterface;

abstract class TestCase extends BaseTestCase
{
    protected Faker $faker;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create(env('APP_LOCALE', 'pt_BR'));
    }

    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * Facilitador para gerar mocks em testes unitários
     *
     * @param string $className
     * @param callable|null $methods
     * @return MockInterface
     */
    protected function mock(string $className, ?callable $methods = null): MockInterface
    {
        $mock = Mockery::mock($className, $methods);
        $this->app->instance($className, $mock);
        return $mock;
    }
}
