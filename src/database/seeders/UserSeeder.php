<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        User::factory([
            'name' => 'Kaique Garcia',
            'birth_date' => '1992-10-31',
            'cpf' => '025.026.515-06',
        ])->create();
    }
}
