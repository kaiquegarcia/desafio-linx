<?php

namespace Database\Seeders;

use App\Models\ATM;
use App\Models\ATMBankNote;
use Illuminate\Database\Seeder;

class ATMBankNoteSeeder extends Seeder
{
    public function run(): void
    {
        $atms = ATM::all();
        $atms->each(function (ATM $atm) {
            ATMBankNote::factory([
                'atm_id' => $atm->id,
                'bank_note' => 20,
                'amount' => 10,
            ])->create();

            ATMBankNote::factory([
                'atm_id' => $atm->id,
                'bank_note' => 50,
                'amount' => 10,
            ])->create();

            ATMBankNote::factory([
                'atm_id' => $atm->id,
                'bank_note' => 100,
                'amount' => 10,
            ])->create();
        });
    }
}
