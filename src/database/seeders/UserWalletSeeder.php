<?php

namespace Database\Seeders;

use App\Enums\WalletType;
use App\Models\User;
use App\Models\UserWallet;
use Illuminate\Database\Seeder;

class UserWalletSeeder extends Seeder
{
    public function run(): void
    {
        $user = User::query()->where('cpf', '025.026.515-06')->first();
        $userId = $user?->id;

        UserWallet::factory([
            'user_id' => $userId,
            'type' => WalletType::SAVINGS()->value,
            'label' => 'Linx Pay (conta poupança)',
            'balance' => 200000.0,
        ])->create();

        UserWallet::factory([
            'user_id' => $userId,
            'type' => WalletType::CHECKING()->value,
            'label' => 'Linx Pay (conta corrente)',
            'balance' => 15000.0,
        ])->create();
    }
}
