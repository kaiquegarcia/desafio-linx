<?php

namespace Database\Seeders;

use App\Models\ATM;
use Illuminate\Database\Seeder;

class ATMSeeder extends Seeder
{
    public function run(): void
    {
        ATM::factory()
            ->count(10)
            ->create();
    }
}
