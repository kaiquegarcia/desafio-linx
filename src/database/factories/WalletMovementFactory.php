<?php

namespace Database\Factories;

use App\Enums\OperationType;
use App\Models\WalletMovement;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\Enum\Laravel\Faker\FakerEnumProvider;

class WalletMovementFactory extends Factory
{
    protected $model = WalletMovement::class;

    public function definition()
    {
        $this->faker->addProvider(new FakerEnumProvider($this->faker));
        return [
            'wallet_id' => $this->faker->uuid(),
            'atm_id' => $this->faker->boolean() ? $this->faker->uuid() : null,
            'operation_type' => $this->faker->randomEnumValue(OperationType::class),
            'amount' => $this->faker->randomNumber(),
        ];
    }
}
