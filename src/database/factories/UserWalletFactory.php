<?php

namespace Database\Factories;

use App\Enums\WalletType;
use App\Models\UserWallet;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\Enum\Laravel\Faker\FakerEnumProvider;

class UserWalletFactory extends Factory
{
    protected $model = UserWallet::class;

    public function definition()
    {
        $this->faker->addProvider(new FakerEnumProvider($this->faker));
        return [
            'user_id' => $this->faker->uuid(),
            'type' => $this->faker->randomEnumValue(WalletType::class),
            'label' => $this->faker->word(),
        ];
    }
}
