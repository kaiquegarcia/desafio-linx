<?php

namespace Database\Factories;

use App\Models\ATM;
use Illuminate\Database\Eloquent\Factories\Factory;

class ATMFactory extends Factory
{
    protected $model = ATM::class;

    public function definition()
    {
        return [
            'label' => $this->faker->word(),
        ];
    }
}
