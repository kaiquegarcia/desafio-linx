<?php

namespace Database\Factories;

use App\Models\ATMBankNote;
use Illuminate\Database\Eloquent\Factories\Factory;

class ATMBankNoteFactory extends Factory
{
    protected $model = ATMBankNote::class;

    public function definition()
    {
        return [
            'atm_id' => $this->faker->uuid(),
            'bank_note' => $this->faker->randomNumber(),
            'amount' => $this->faker->randomNumber(),
        ];
    }
}
