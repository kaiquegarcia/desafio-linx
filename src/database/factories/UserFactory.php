<?php

namespace Database\Factories;

use App\Models\User;
use Faker\Provider\pt_BR\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->faker->addProvider(new Person($this->faker));
        return [
            'name' => $this->faker->name,
            'cpf' => $this->faker->cpf(),
            'birth_date' => $this->faker->date('Y-m-d', 'now - 18 years'),
        ];
    }
}
