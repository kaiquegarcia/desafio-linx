<?php

namespace App\Http\Requests;

use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;

class WithdrawRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        if ($this->route('id')) {
            $this->merge(['wallet_id' => $this->route('id')]);
        }
        return $this->input();
    }

    #[ArrayShape(['wallet_id' => "string", 'atm_id' => "string", 'amount' => "string"])]
    public function rules(): array
    {
        return [
            'wallet_id' => 'required|string|uuid',
            'atm_id' => 'required|string|uuid',
            'amount' => 'required|integer',
        ];
    }

    public function messages(): array
    {
        return [
            'wallet_id.required' => 'O id da carteira é obrigatório.',
            'wallet_id.string' => 'O id da carteira deve ser um texto.',
            'wallet_id.uuid' => 'O id da carteira deve ser UUID.',
            'atm_id.required' => 'O id do caixa eletrônico é obrigatório.',
            'atm_id.string' => 'O id do caixa eletrônico deve ser um texto.',
            'atm_id.uuid' => 'O id do caixa eletrônico deve ser UUID.',
            'amount.required' => 'Informe a quantia a ser retirada.',
            'amount.integer' => 'A quantia a retirar só pode ser inteira, sem centavos.',
        ];
    }
}
