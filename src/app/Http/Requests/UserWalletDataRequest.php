<?php

namespace App\Http\Requests;

use App\DTOs\UserWalletDTO;
use App\Enums\WalletType;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Spatie\Enum\Laravel\Rules\EnumRule;

class UserWalletDataRequest extends RequestAbstract
{

    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        $pathInput = ['user_id' => $this->route('user_id')];
        if ($this->route('id')) {
            $pathInput['id'] = $this->route('id');
        }
        $this->merge($pathInput);
        return $this->input();
    }

    private function getUniquePerUserRule(): Unique
    {
        $userId = $this->get('user_id');
        $walletId = $this->get('id') ?? null;
        return Rule::unique('user_wallets')->where(function ($query) use ($userId, $walletId) {
            $query->whereNull('deleted_at');
            $query->where('user_id', $userId);
            if ($walletId && Str::isUuid($walletId)) {
                $query->where('id', '<>', $walletId);
            }
        });
    }

    #[ArrayShape(['id' => 'string', 'user_id' => "string", 'type' => "array", 'label' => "array", 'balance' => "string"])]
    public function rules(): array
    {
        return [
            'id' => 'string|uuid',
            'user_id' => 'required|string|uuid',
            'type' => [
                'required',
                Rule::in(WalletType::toValues()),
                $this->getUniquePerUserRule(),
            ],
            'label' => [
                'required',
                'string',
                $this->getUniquePerUserRule(),
            ],
            'balance' => 'numeric|min:0',
        ];
    }

    public function messages(): array
    {
        return [
            'user_id.required' => 'O campo "user_id" é obrigatório.',
            'type.required' => 'O campo "type" é obrigatório.',
            'label.required' => 'O campo "título" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
            'user_id.string' => 'O campo "user_id" deve ser um texto.',
            'user_id.uuid' => 'O campo "user_id" deve ser UUID.',
            'type.unique' => 'Esse usuário já tem uma carteira desse tipo.',
            'type.in' => 'Tipo de carteira inválido.',
            'label.string' => 'O campo "título" deve ser um texto.',
            'label.unique' => 'Esse usuário já tem uma carteira com esse título.',
            'balance.numeric' => 'O campo "balance" precisa ser numérico.',
            'balance.min' => 'O campo "balance" não pode ser abaixo de zero.',
        ];
    }

    /**
     * @throws UnknownProperties
     */
    public function getDto(): UserWalletDTO
    {
        return new UserWalletDTO(
            type: WalletType::tryFrom($this->get('type')),
            label: $this->get('label'),
            balance: $this->get('balance')
        );
    }
}
