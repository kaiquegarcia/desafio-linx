<?php

namespace App\Http\Requests;

use App\DTOs\UserWalletDTO;
use App\Enums\WalletType;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Spatie\Enum\Laravel\Http\Requests\TransformsEnums;
use Spatie\Enum\Laravel\Rules\EnumRule;

class UserWalletDeleteRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        $pathInput = ['user_id' => $this->route('user_id')];
        if ($this->route('id')) {
            $pathInput['id'] = $this->route('id');
        }
        $this->merge($pathInput);
        return $this->input();
    }

    #[ArrayShape(['id' => 'string', 'user_id' => 'string'])]
    public function rules(): array
    {
        return [
            'id' => 'required|string|uuid',
            'user_id' => 'required|string|uuid',
        ];
    }

    public function messages(): array
    {
        return [
            'id.required' => 'O campo "id" é obrigatório.',
            'user_id.required' => 'O campo "user_id" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
            'user_id.string' => 'O campo "user_id" deve ser um texto.',
            'user_id.uuid' => 'O campo "user_id" deve ser UUID.',
        ];
    }
}
