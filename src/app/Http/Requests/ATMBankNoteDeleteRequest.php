<?php

namespace App\Http\Requests;

use App\DTOs\UserWalletDTO;
use App\Enums\WalletType;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Spatie\Enum\Laravel\Http\Requests\TransformsEnums;
use Spatie\Enum\Laravel\Rules\EnumRule;

class ATMBankNoteDeleteRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        $pathInput = ['atm_id' => $this->route('atm_id')];
        if ($this->route('id')) {
            $pathInput['id'] = $this->route('id');
        }
        $this->merge($pathInput);
        return $this->input();
    }

    #[ArrayShape(['id' => 'string', 'atm_id' => 'string'])]
    public function rules(): array
    {
        return [
            'id' => 'required|string|uuid',
            'atm_id' => 'required|string|uuid',
        ];
    }

    public function messages(): array
    {
        return [
            'id.required' => 'O campo "id" é obrigatório.',
            'atm_id.required' => 'O campo "atm_id" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
            'atm_id.string' => 'O campo "atm_id" deve ser um texto.',
            'atm_id.uuid' => 'O campo "atm_id" deve ser UUID.',
        ];
    }
}
