<?php

namespace App\Http\Requests;

use App\DTOs\ATMDTO;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class ATMDataRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        if ($this->route('id')) {
            $this->merge(['id' => $this->route('id')]);
        }
        return $this->input();
    }

    private function getLabelUniqueRule(): Unique
    {
        $atmId = $this->get('id') ?? null;
        return Rule::unique('atms')->where(function ($query) use ($atmId) {
            $query->whereNull('deleted_at');
            if ($atmId && Str::isUuid($atmId)) {
                $query->where('id', '<>', $atmId);
            }
        });
    }

    #[ArrayShape(['id' => 'string', 'label' => "array"])]
    public function rules(): array
    {
        return [
            'id' => 'string|uuid',
            'label' => [
                'required',
                'string',
                $this->getLabelUniqueRule(),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'label.required' => 'O campo "título" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
            'label.string' => 'O campo "título" deve ser um texto.',
            'label.unique' => 'Esse "título" já está em uso.',
        ];
    }

    /**
     * @throws UnknownProperties
     */
    public function getDto(): ATMDTO
    {
        return new ATMDTO(
            label: $this->get('label')
        );
    }
}
