<?php

namespace App\Http\Requests;

use App\DTOs\ATMBankNoteDTO;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class ATMBankNoteDataRequest extends RequestAbstract
{

    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        $pathInput = ['atm_id' => $this->route('atm_id')];
        if ($this->route('id')) {
            $pathInput['id'] = $this->route('id');
        }
        $this->merge($pathInput);
        return $this->input();
    }

    private function getUniquePerATMRule(): Unique
    {
        $atmId = $this->get('atm_id');
        $walletId = $this->get('id') ?? null;
        return Rule::unique('atm_bank_notes')->where(function ($query) use ($atmId, $walletId) {
            $query->whereNull('deleted_at');
            $query->where('atm_id', $atmId);
            if ($walletId && Str::isUuid($walletId)) {
                $query->where('id', '<>', $walletId);
            }
        });
    }

    #[ArrayShape(['id' => "string", 'atm_id' => "string", 'bank_note' => "array", 'amount' => "string"])]
    public function rules(): array
    {
        return [
            'id' => 'string|uuid',
            'atm_id' => 'required|string|uuid',
            'bank_note' => [
                'required',
                'numeric',
                'min:1',
                $this->getUniquePerATMRule(),
            ],
            'amount' => 'required|numeric|min:1|regex:/^(\d{1,11})$/',
        ];
    }

    public function messages(): array
    {
        return [
            'atm_id.required' => 'O campo "atm_id" é obrigatório.',
            'bank_note.required' => 'O campo "bank_note" é obrigatório.',
            'amount.required' => 'O campo "amount" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
            'atm_id.string' => 'O campo "atm_id" deve ser um texto.',
            'atm_id.uuid' => 'O campo "atm_id" deve ser UUID.',
            'bank_note.unique' => 'Essa nota já foi registrada nesse ATM.',
            'bank_note.numeric' => 'O campo "bank_note" precisa ser numérico.',
            'bank_note.min' => 'O campo "bank_note" não pode ser menor que 1 (um).',
            'amount.numeric' => 'O campo "amount" precisa ser numérico.',
            'amount.min' => 'O campo "amount" não pode ser menor que 1 (um).',
        ];
    }

    /**
     * @throws UnknownProperties
     */
    public function getDto(): ATMBankNoteDTO
    {
        return new ATMBankNoteDTO(
            bank_note: $this->get('bank_note'),
            amount: $this->get('amount')
        );
    }
}
