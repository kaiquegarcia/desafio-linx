<?php

namespace App\Http\Requests;

use App\DTOs\ATMDTO;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class ModelDeleteRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        if ($this->route('id')) {
            $this->merge(['id' => $this->route('id')]);
        }
        return $this->input();
    }

    #[ArrayShape(['id' => 'string'])]
    public function rules(): array
    {
        return [
            'id' => 'required|string|uuid',
        ];
    }

    public function messages(): array
    {
        return [
            'id.required' => 'O campo "id" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
        ];
    }
}
