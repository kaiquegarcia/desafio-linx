<?php

namespace App\Http\Requests;

use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;

class DepositRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        if ($this->route('id')) {
            $this->merge(['wallet_id' => $this->route('id')]);
        }
        return $this->input();
    }

    #[ArrayShape(['wallet_id' => "string", 'amount' => "string"])]
    public function rules(): array
    {
        return [
            'wallet_id' => 'required|string|uuid',
            'amount' => 'required|integer|min:1',
        ];
    }

    public function messages(): array
    {
        return [
            'wallet_id.required' => 'O id da carteira é obrigatório.',
            'wallet_id.string' => 'O id da carteira deve ser um texto.',
            'wallet_id.uuid' => 'O id da carteira deve ser UUID.',
            'amount.required' => 'Informe a quantia a ser depositada.',
            'amount.integer' => 'A quantia a depositar só pode ser inteira, sem centavos.',
            'amount.min' => 'A menor quantidade a depositar deve ser R$1,00.',
        ];
    }
}
