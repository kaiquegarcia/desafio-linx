<?php

namespace App\Http\Requests;

use App\DTOs\UserDTO;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use JetBrains\PhpStorm\ArrayShape;
use Pearl\RequestValidate\RequestAbstract;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class UserDataRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function validationData(): array
    {
        if ($this->route('id')) {
            $this->merge(['id' => $this->route('id')]);
        }
        return $this->input();
    }

    private function getCpfUniqueRule(): Unique
    {
        $userId = $this->get('id') ?? null;
        return Rule::unique('users')->where(function ($query) use ($userId) {
            $query->whereNull('deleted_at');
            if ($userId && Str::isUuid($userId)) {
                $query->where('id', '<>', $userId);
            }
        });
    }

    #[ArrayShape(['id' => 'string', 'name' => "string", 'birth_date' => "string", 'cpf' => "array"])]
    public function rules(): array
    {
        return [
            'id' => 'string|uuid',
            'name' => 'required|string',
            'birth_date' => 'required|date_format:Y-m-d',
            'cpf' => [
                'required',
                'cpf',
                'regex:/^(\d{3}).(\d{3}).(\d{3})-(\d{2})$/',
                $this->getCpfUniqueRule(),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'O campo "nome" é obrigatório.',
            'birth_date.required' => 'O campo "data de nascimento" é obrigatório.',
            'cpf.required' => 'O campo "CPF" é obrigatório.',
            'id.string' => 'O campo "id" deve ser um texto.',
            'id.uuid' => 'O campo "id" deve ser UUID.',
            'name.string' => 'O campo "nome" Deve ser um texto.',
            'cpf.cpf' => 'O campo "CPF" deve ser um CPF válido.',
            'cpf.regex' => 'O campo "CPF" deve seguir o formato padrão: 123.456.789-00.',
            'cpf.unique' => 'Esse "CPF" já está em uso.',
        ];
    }

    /**
     * @throws UnknownProperties
     */
    public function getDto(): UserDTO
    {
        return new UserDTO(
            name: $this->get('name'),
            birth_date: $this->get('birth_date'),
            cpf: $this->get('cpf')
        );
    }
}
