<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserAuth
{
    public function handle(Request $request, Closure $next)
    {
        $userId = $request->header('USER_ID');
        $user = $userId && Str::isUuid($userId) ? User::query()->find($userId)->first() : null;
        if (!$user) {
            return response('Unauthorized.', 401);
        }

        $request->merge(['user_id' => $userId, 'user' => $user]);

        return $next($request);
    }
}
