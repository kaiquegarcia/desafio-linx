<?php

namespace App\Http\Validators;

use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

class UuidValidator extends Validator
{
    public function validateUuid($attribute, $value): bool
    {
        return Str::isUuid($value);
    }

}
