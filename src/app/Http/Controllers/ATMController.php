<?php

namespace App\Http\Controllers;

use App\Exceptions\ATMNotFoundException;
use App\Http\Requests\ATMDataRequest;
use App\Http\Requests\ModelDeleteRequest;
use App\Services\ATMService;
use Exception;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class ATMController extends BaseController
{
    public function __construct(
        private ATMService $atmService
    ) {}

    public function all(): JsonResponse
    {
        return response()->json($this->atmService->all());
    }

    public function find(string $id): JsonResponse
    {
        try {
            $atm = $this->atmService->find($id);
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'ATM não encontrado'], 204);
        }
        return response()->json($atm);
    }

    public function store(ATMDataRequest $request): JsonResponse
    {
        try {
            $this->atmService->store($request->getDto());
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'ATM cadastrado com sucesso.']);
    }

    public function update(ATMDataRequest $request, string $id): JsonResponse
    {
        try {
            $this->atmService->update($id, $request->getDto());
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'ATM não encontrado'], 204);
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'ATM alterado com sucesso.']);
    }

    public function delete(ModelDeleteRequest $request, string $id): JsonResponse
    {
        try {
            $this->atmService->delete($id);
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'ATM não encontrado'], 204);
        }
        return response()->json(['message' => 'ATM excluído com sucesso.']);
    }
}
