<?php

namespace App\Http\Controllers;

use App\Exceptions\ATMNotFoundException;
use App\Exceptions\InsufficientBalanceException;
use App\Exceptions\InsufficientBankNotesException;
use App\Exceptions\UnavailableBankNotesException;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\UserWalletNotFoundException;
use App\Http\Requests\DepositRequest;
use App\Http\Requests\UserWalletDataRequest;
use App\Http\Requests\UserWalletDeleteRequest;
use App\Http\Requests\WithdrawRequest;
use App\Services\ATMService;
use App\Services\DepositService;
use App\Services\UserWalletService;
use App\Services\WalletMovementService;
use App\Services\WithdrawService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class UserWalletController extends BaseController
{
    public function __construct(
        private UserWalletService $walletService,
        private WalletMovementService $movementService,
        private DepositService $depositService,
        private ATMService $atmService,
        private WithdrawService $withdrawService
    ) {}

    public function all(string $user_id): JsonResponse
    {
        try {
            $wallets = response()->json($this->walletService->all($user_id));
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        }
        return $wallets;
    }

    public function allFromUser(Request $request): JsonResponse
    {
        try {
            $wallets = response()->json($this->walletService->allFromUser($request->get('user')));
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        }
        return $wallets;
    }

    public function find(string $user_id, string $id): JsonResponse
    {
        try {
            $wallet = $this->walletService->find($user_id, $id);
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        }
        return response()->json($wallet);
    }

    public function findFromUser(Request $request, string $id): JsonResponse
    {
        try {
            $wallet = $this->walletService->findFromUser($request->get('user'), $id);
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        }
        return response()->json($wallet);
    }

    public function store(UserWalletDataRequest $request, string $user_id): JsonResponse
    {
        try {
            $this->walletService->store($user_id, $request->getDto());
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'Carteira cadastrada com sucesso.']);
    }

    public function update(UserWalletDataRequest $request, string $user_id, string $id): JsonResponse
    {
        try {
            $this->walletService->update($user_id, $id, $request->getDto());
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'Carteira alterada com sucesso.']);
    }

    public function delete(UserWalletDeleteRequest $request, string $user_id, string $id): JsonResponse
    {
        try {
            $this->walletService->delete($user_id, $id);
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        }
        return response()->json(['message' => 'Carteira excluída com sucesso.']);
    }

    public function allMovements(Request $request, string $id): JsonResponse
    {
        try {
            $wallet = $this->walletService->findFromUser($request->get('user'), $id);
            $movements = $this->movementService->all($wallet);
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        }
        return response()->json($movements);
    }

    public function deposit(DepositRequest $request, string $id): JsonResponse
    {
        try {
            $wallet = $this->walletService->findFromUser($request->get('user'), $id);
            $this->depositService->deposit($wallet, $request->get('amount'));
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        }
        return response()->json(['message' => 'Depósito efetuado com sucesso.']);
    }

    public function withdraw(WithdrawRequest $request, string $id): JsonResponse
    {
        try {
            $wallet = $this->walletService->findFromUser($request->get('user'), $id);
            $atm = $this->atmService->find($request->get('atm_id'));
            $bankNotes = $this->withdrawService->withdraw($atm, $wallet, $request->get('amount'));
        } catch (UserWalletNotFoundException $exception) {
            return response()->json(['error' => 'Carteira não encontrada'], 204);
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'Caixa eletrônico não encontrado'], 204);
        } catch (InsufficientBalanceException $e) {
            return response()->json(['error' => 'Saldo insuficiente'], 400);
        } catch (InsufficientBankNotesException $e) {
            return response()->json(['error' => 'Não há cédulas o suficiente para registrar a saída'], 400);
        } catch (UnavailableBankNotesException $e) {
            return response()->json(['error' => 'O valor inserido é menor que a menor das cédulas'], 400);
        }
        return response()->json(['message' => 'Saque efetuado com sucesso!', 'bank_notes' => $bankNotes->toArray()]);
    }
}
