<?php

namespace App\Http\Controllers;

use App\Exceptions\UserNotFoundException;
use App\Http\Requests\ModelDeleteRequest;
use App\Http\Requests\UserDataRequest;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
    public function __construct(
        private UserService $userService
    ) {}

    public function all(): JsonResponse
    {
        return response()->json($this->userService->all());
    }

    public function find(string $id): JsonResponse
    {
        try {
            $user = $this->userService->find($id);
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        }
        return response()->json($user);
    }

    public function store(UserDataRequest $request): JsonResponse
    {
        try {
            $this->userService->store($request->getDto());
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'Usuário cadastrado com sucesso.']);
    }

    public function update(UserDataRequest $request, string $id): JsonResponse
    {
        try {
            $this->userService->update($id, $request->getDto());
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'Usuário alterado com sucesso.']);
    }

    public function delete(ModelDeleteRequest $request, string $id): JsonResponse
    {
        try {
            $this->userService->delete($id);
        } catch (UserNotFoundException $exception) {
            return response()->json(['error' => 'Usuário não encontrado'], 204);
        }
        return response()->json(['message' => 'Usuário excluído com sucesso.']);
    }
}
