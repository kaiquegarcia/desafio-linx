<?php

namespace App\Http\Controllers;

use App\Exceptions\ATMBankNoteNotFoundException;
use App\Exceptions\ATMNotFoundException;
use App\Http\Requests\ATMBankNoteDataRequest;
use App\Http\Requests\ATMBankNoteDeleteRequest;
use App\Services\ATMBankNoteService;
use Exception;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class ATMBankNoteController extends BaseController
{
    public function __construct(
        private ATMBankNoteService $bankNoteService
    ) {}

    public function all(string $atm_id): JsonResponse
    {
        try {
            return response()->json($this->bankNoteService->all($atm_id));
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'Caixa eletrônico não encontrado.'], 204);
        }
    }

    public function find(string $atm_id, string $id): JsonResponse
    {
        try {
            $bankNote = $this->bankNoteService->find($atm_id, $id);
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'Caixa eletrônico não encontrado.'], 204);
        } catch (ATMBankNoteNotFoundException $exception) {
            return response()->json(['error' => 'Cédula não encontrada.'], 204);
        }
        return response()->json($bankNote);
    }

    public function store(ATMBankNoteDataRequest $request, string $atm_id): JsonResponse
    {
        try {
            $this->bankNoteService->store($atm_id, $request->getDto());
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'Cédula cadastrada com sucesso.']);
    }

    public function update(ATMBankNoteDataRequest $request, string $atm_id, string $id): JsonResponse
    {
        try {
            $this->bankNoteService->update($atm_id, $id, $request->getDto());
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'Caixa eletrônico não encontrado.'], 204);
        } catch (ATMBankNoteNotFoundException $exception) {
            return response()->json(['error' => 'Cédula não encontrada.'], 204);
        } catch (Exception $exception) {
            return response()->json(['error' => 'Ocorreu um erro interno.'], 502);
        }
        return response()->json(['message' => 'Cédula alterada com sucesso.']);
    }

    public function delete(ATMBankNoteDeleteRequest $request, string $atm_id, string $id): JsonResponse
    {
        try {
            $this->bankNoteService->delete($atm_id, $id);
        } catch (ATMNotFoundException $exception) {
            return response()->json(['error' => 'Caixa eletrônico não encontrado.'], 204);
        } catch (ATMBankNoteNotFoundException $exception) {
            return response()->json(['error' => 'Cédula não encontrada.'], 204);
        }
        return response()->json(['message' => 'Carteira excluída com sucesso.']);
    }
}
