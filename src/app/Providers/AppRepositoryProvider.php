<?php

namespace App\Providers;

use App\Repositories\Redis\WalletMovementRedisRepository;
use App\Repositories\WalletMovementRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(WalletMovementRepositoryInterface::class, WalletMovementRedisRepository::class);
    }
}
