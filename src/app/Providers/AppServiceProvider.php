<?php

namespace App\Providers;

use App\Http\Validators\UuidValidator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->resolver(function (
            $translator,
            $data,
            $rules,
            $messages,
            $attributes
        ) {
            return new UuidValidator(
                $translator,
                $data,
                $rules,
                $messages,
                $attributes
            );
        });
    }

    public function register()
    {
    }
}
