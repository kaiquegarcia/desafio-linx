<?php

namespace App\Repositories\Redis;

use Illuminate\Support\Facades\Redis;
use JetBrains\PhpStorm\Pure;
use Predis\Response\Status;

abstract class RedisRepository
{
    private mixed $client;

    #[Pure]
    public function __construct(
        Redis $redis
    ) {
        $this->client = $redis::connection()->client();
    }

    abstract protected function mountKey(mixed $keyInput): string;

    protected function has(string $keyInput): bool
    {
        return $this->client->has($this->mountKey($keyInput));
    }

    protected function get(string $keyInput): mixed
    {
        return $this->client->get($this->mountKey($keyInput));
    }

    protected function set(
        string $keyInput,
        string $value
    ): bool
    {
        /** @var Status $result */
        $result = $this->client->set(
            $this->mountKey($keyInput),
            $value
        );
        return $result->getPayload() === 'OK';
    }

    protected function delete(string $keyInput): bool
    {
        $deletedCount = $this->client->del($this->mountKey($keyInput));
        return $deletedCount > 0;
    }
}
