<?php

namespace App\Repositories\Redis;

use App\Cache\WalletMovement;
use App\Repositories\WalletMovementRepositoryInterface;
use Illuminate\Support\Collection;

class WalletMovementRedisRepository extends RedisRepository implements WalletMovementRepositoryInterface
{

    protected function mountKey(mixed $keyInput): string
    {
        return "wallet_{$keyInput}_movements";
    }

    public function all(string $wallet_id): Collection
    {
        $movements = $this->get($wallet_id);
        $movementJsons = empty($movements) ? [] : json_decode($movements);
        $movements = [];
        foreach($movementJsons as $json) {
            $movements[] = WalletMovement::jsonUnserialize($json);
        }
        return collect($movements);
    }

    public function find(string $wallet_id, string $movement_id): ?WalletMovement
    {
        $movements = $this->all($wallet_id);
        $movements->filter(
            fn(WalletMovement $movement) =>
                $movement->getId() === $movement_id
        );
        if (empty($movements)) {
            return null;
        }
        return $movements[0];
    }

    public function save(WalletMovement $movement): bool
    {
        if (!$movement->getId()) {
            $movement->prePersist();
        }
        $movements = $this->all($movement->getWalletId());
        $movements->push($movement);
        return $this->set($movement->getWalletId(), json_encode($movements->toArray()));
    }

    public function remove(string $walletId, string $movementId): bool
    {
        $movements = $this->all($walletId);
        $movements->filter(fn(WalletMovement $movement) => $movement->getId() !== $movementId);
        return $this->set($walletId, json_encode($movements->toArray()));
    }
}
