<?php

namespace App\Repositories;

use App\Cache\WalletMovement;
use Illuminate\Support\Collection;

interface WalletMovementRepositoryInterface
{
    public function all(string $wallet_id): Collection;
    public function find(string $wallet_id, string $movement_id): ?WalletMovement;
    public function save(WalletMovement $movement): bool;
}
