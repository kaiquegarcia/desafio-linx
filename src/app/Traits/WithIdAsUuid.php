<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait WithIdAsUuid
{
    public static function bootWithIdAsUuid(): void
    {
        static::creating(function (Model $model) {
            $model->id = Str::uuid();
        });
    }
}
