<?php

namespace App\Traits;

use App\Services\WalletMovementService;
use Illuminate\Database\Eloquent\Model;

trait RegisterOpeningBalanceAfterCreated
{
    public static function bootRegisterOpeningBalanceAfterCreated(): void
    {
        static::created(function(Model $wallet) {
            $movementService = app(WalletMovementService::class);
            $movementService->openBalance(
                walletId: $wallet->id,
                balance: $wallet->balance
            );
        });
    }
}
