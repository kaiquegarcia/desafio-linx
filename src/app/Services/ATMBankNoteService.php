<?php

namespace App\Services;

use App\DTOs\ATMBankNoteDTO;
use App\DTOs\WithdrawBankNoteDTO;
use App\Exceptions\ATMBankNoteNotFoundException;
use App\Exceptions\ATMNotFoundException;
use App\Models\ATMBankNote;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

class ATMBankNoteService
{
    public function __construct(
        private ATMService $atmService
    ) {
    }

    /**
     * @param string $atmId
     * @return Collection
     * @throws ATMNotFoundException
     */
    public function all(string $atmId): Collection
    {
        $atm = $this->atmService->find($atmId);
        return $atm->bank_notes()->get()->sortByDesc('bank_note');
    }

    /**
     * @param string $atmId
     * @param string $id
     * @return ATMBankNote
     * @throws ATMBankNoteNotFoundException
     * @throws ATMNotFoundException
     */
    public function find(string $atmId, string $id): ATMBankNote
    {
        $atm = $this->atmService->find($atmId);
        /** @var ATMBankNote $bankNote */
        $bankNote = $atm->bank_notes()->where('id', $id)->first();
        if (!$bankNote) {
            throw new ATMBankNoteNotFoundException();
        }
        return $bankNote;
    }

    /**
     * @param string $atmId
     * @param ATMBankNoteDTO $dto
     * @return ATMBankNote
     * @throws Throwable
     */
    public function store(string $atmId, ATMBankNoteDTO $dto): ATMBankNote
    {
        $bankNote = new ATMBankNote();
        $bankNote->atm_id = $atmId;
        $bankNote->bank_note = $dto->bank_note;
        $bankNote->amount = $dto->amount;
        $bankNote->saveOrFail();

        return $bankNote;
    }

    /**
     * @param string $atmId
     * @param string $id
     * @param ATMBankNoteDTO $dto
     * @return ATMBankNote
     * @throws ATMBankNoteNotFoundException
     * @throws ATMNotFoundException
     * @throws Throwable
     */
    public function update(string $atmId, string $id, ATMBankNoteDTO $dto): ATMBankNote
    {
        $bankNote = $this->find($atmId, $id);
        $bankNote->bank_note = $dto->bank_note;
        $bankNote->amount = $dto->amount;
        $bankNote->saveOrFail();

        return $bankNote;
    }

    /**
     * @param string $atmId
     * @param string $id
     * @throws ATMBankNoteNotFoundException
     * @throws ATMNotFoundException
     */
    public function delete(string $atmId, string $id): void
    {
        $bankNote = $this->find($atmId, $id);
        $bankNote->delete();
    }

    /**
     * @param WithdrawBankNoteDTO $dto
     */
    public function subtractAmount(WithdrawBankNoteDTO $dto): void
    {
        $bankNote = ATMBankNote::find($dto->id);
        $bankNote->amount -= $dto->amount;
        $bankNote->saveOrFail();
    }
}
