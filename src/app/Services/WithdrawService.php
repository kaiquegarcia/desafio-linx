<?php

namespace App\Services;

use App\DTOs\ATMBankNoteDTO;
use App\DTOs\WithdrawBankNoteDTO;
use App\Enums\OperationType;
use App\Exceptions\InsufficientBalanceException;
use App\Exceptions\InsufficientBankNotesException;
use App\Exceptions\UnavailableBankNotesException;
use App\Models\ATM;
use App\Models\ATMBankNote;
use App\Models\UserWallet;
use App\Repositories\WalletMovementRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class WithdrawService extends WalletMovementService
{
    public function __construct(
        WalletMovementRepositoryInterface $repository,
        private ATMBankNoteService $bankNoteService
    )
    {
        parent::__construct($repository);
    }

    /**
     * @param UserWallet $wallet
     * @param int $amount
     * @throws InsufficientBalanceException
     */
    private function checkWalletBalance(UserWallet $wallet, int $amount): void
    {
        if ($wallet->balance < $amount) {
            throw new InsufficientBalanceException();
        }
    }

    /**
     * @param ATM $atm
     * @return Collection
     */
    private function getAvailableBankNotes(ATM $atm): Collection
    {
        return $atm->bank_notes()
            ->get()
            ->sortByDesc('bank_note')
            ->filter(fn(ATMBankNote $bankNote) => $bankNote->amount > 0);
    }

    /**
     * @param Collection $bankNotes
     * @param int $amount
     * @throws UnavailableBankNotesException
     */
    private function checkAmountIsLowerThanTheLowerBankNote(Collection $bankNotes, int $amount): void
    {
        if ($amount < $bankNotes->last()->bank_note) {
            throw new UnavailableBankNotesException();
        }
    }

    /**
     * @param Collection $bankNotes
     * @param int $amount
     * @return Collection
     * @throws UnknownProperties
     */
    private function getBankNotesForAmount(Collection $bankNotes, int $amount): Collection
    {
        $withdrawBankNotes = collect();
        $leftAmount = $amount;
        $bankNotes->each(function(ATMBankNote $bankNote) use (&$withdrawBankNotes, &$leftAmount) {
            $portions = floor($leftAmount / $bankNote->bank_note);
            if (!$portions) {
                return;
            }
            if ($portions > $bankNote->amount) {
                $portions = $bankNote->amount;
            }
            $withdrawBankNotes->push(new WithdrawBankNoteDTO(
                id: $bankNote->id,
                atm_id: $bankNote->atm_id,
                bank_note: $bankNote->bank_note,
                amount: $portions,
            ));
            $leftAmount -= ($bankNote->bank_note * $portions);
        });
        return $withdrawBankNotes;
    }

    /**
     * @param Collection $withdrawBankNotes
     * @param int $amount
     * @throws InsufficientBankNotesException
     */
    private function checkSumOfBankNotesIsEqualThanAmount(Collection $withdrawBankNotes, int $amount): void
    {
        $total = $withdrawBankNotes->reduce(fn($acc, WithdrawBankNoteDTO $dto) => $acc + ($dto->bank_note * $dto->amount));
        if ($amount != $total) {
            throw new InsufficientBankNotesException();
        }
    }

    private function persistBankNotesAmountOnDatabase(Collection $withdrawBankNotes): void
    {
        $withdrawBankNotes->each(function(WithdrawBankNoteDTO $dto) {
            $this->bankNoteService->subtractAmount($dto);
        });
    }

    /**
     * @param ATM $atm
     * @param UserWallet $wallet
     * @param int $amount
     * @return Collection
     * @throws InsufficientBalanceException
     * @throws InsufficientBankNotesException
     * @throws UnavailableBankNotesException
     * @throws UnknownProperties
     */
    public function withdraw(
        ATM $atm,
        UserWallet $wallet,
        int $amount
    ): Collection {
        $this->checkWalletBalance($wallet, $amount);
        $bankNotes = $this->getAvailableBankNotes($atm);
        $this->checkAmountIsLowerThanTheLowerBankNote($bankNotes, $amount);
        $withdrawBankNotes = $this->getBankNotesForAmount($bankNotes, $amount);
        $this->checkSumOfBankNotesIsEqualThanAmount($withdrawBankNotes, $amount);
        $this->persistBankNotesAmountOnDatabase($withdrawBankNotes);
        $this->subtract($wallet, $amount, OperationType::WITHDRAW());
        return $withdrawBankNotes;
    }
}
