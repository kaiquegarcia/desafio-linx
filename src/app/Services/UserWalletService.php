<?php

namespace App\Services;

use App\DTOs\UserWalletDTO;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\UserWalletNotFoundException;
use App\Models\User;
use App\Models\UserWallet;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

class UserWalletService
{
    public function __construct(
        private UserService $userService,
        private WalletMovementService $movementService
    ) { }

    /**
     * @param string $userId
     * @return Collection
     * @throws UserNotFoundException
     */
    public function all(string $userId): Collection
    {
        $user = $this->userService->find($userId);
        return $this->allFromUser($user);
    }

    /**
     * @param User $user
     * @return Collection
     */
    public function allFromUser(User $user): Collection
    {
        return $user->wallets()->get();
    }

    /**
     * @param string $userId
     * @param string $id
     * @return UserWallet
     * @throws UserNotFoundException
     * @throws UserWalletNotFoundException
     */
    public function find(string $userId, string $id): UserWallet
    {
        $user = $this->userService->find($userId);
        return $this->findFromUser($user, $id);
    }

    /**
     * @param User $user
     * @param string $id
     * @return UserWallet
     * @throws UserWalletNotFoundException
     */
    public function findFromUser(User $user, string $id): UserWallet
    {
        /** @var UserWallet $wallet */
        $wallet = $user->wallets()->where('id', $id)->first();
        if (!$wallet) {
            throw new UserWalletNotFoundException();
        }
        return $wallet;
    }

    /**
     * @param string $userId
     * @param UserWalletDTO $dto
     * @return UserWallet
     * @throws Throwable
     */
    public function store(string $userId, UserWalletDTO $dto): UserWallet
    {
        $wallet = new UserWallet();
        $wallet->user_id = $userId;
        $wallet->type = $dto->type;
        $wallet->label = $dto->label;
        $wallet->balance = floatval($dto->balance);
        $wallet->saveOrFail();
        return $wallet;
    }

    /**
     * @param string $userId
     * @param string $id
     * @param UserWalletDTO $dto
     * @return UserWallet
     * @throws Throwable
     * @throws UserNotFoundException
     * @throws UserWalletNotFoundException
     */
    public function update(string $userId, string $id, UserWalletDTO $dto): UserWallet
    {
        $hasBalanceModifications = !is_null($dto->balance);
        $wallet = $this->find($userId, $id);
        $wallet->type = $dto->type;
        $wallet->label = $dto->label;
        if ($hasBalanceModifications) {
            $previousBalance = $wallet->balance;
            $wallet->balance = $dto->balance;
        }
        $wallet->saveOrFail();

        if ($hasBalanceModifications) {
            $this->movementService->reBalance(
                walletId: $wallet->id,
                previousBalance: $previousBalance,
                newBalance: $wallet->balance
            );
        }
        return $wallet;
    }

    /**
     * @param string $userId
     * @param string $id
     * @throws UserNotFoundException
     * @throws UserWalletNotFoundException
     */
    public function delete(string $userId, string $id): void
    {
        $wallet = $this->find($userId, $id);
        $wallet->delete();
    }
}
