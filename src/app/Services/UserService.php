<?php

namespace App\Services;

use App\DTOs\UserDTO;
use App\Exceptions\UserNotFoundException;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

class UserService
{
    /**
     * @return Collection|array
     */
    public function all(): Collection|array
    {
        return User::all();
    }

    /**
     * @param string $id
     * @return User
     * @throws UserNotFoundException
     */
    public function find(string $id): User
    {
        $user = User::find($id);
        if (!$user) {
            throw new UserNotFoundException();
        }
        return $user;
    }

    /**
     * @param UserDTO $dto
     * @return User
     * @throws Throwable
     */
    public function store(UserDTO $dto): User
    {
        $user = new User();
        $user->name = $dto->name;
        $user->birth_date = $dto->birth_date;
        $user->cpf = $dto->cpf;
        $user->saveOrFail();
        return $user;
    }

    /**
     * @param string $id
     * @param UserDTO $dto
     * @return User
     * @throws Throwable
     * @throws UserNotFoundException
     */
    public function update(string $id, UserDTO $dto): User
    {
        $user = $this->find($id);
        $user->name = $dto->name;
        $user->birth_date = $dto->birth_date;
        $user->cpf = $dto->cpf;
        $user->saveOrFail();
        return $user;
    }

    /**
     * @param string $id
     * @throws UserNotFoundException
     */
    public function delete(string $id): void
    {
        $user = $this->find($id);
        $user->delete();
    }
}
