<?php

namespace App\Services;

use App\DTOs\ATMDTO;
use App\Exceptions\ATMNotFoundException;
use App\Models\ATM;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

class ATMService
{
    /**
     * @return Collection|array
     */
    public function all(): Collection|array
    {
        return ATM::all();
    }

    /**
     * @param string $id
     * @return ATM
     * @throws ATMNotFoundException
     */
    public function find(string $id): ATM
    {
        $atm = ATM::find($id);
        if (!$atm) {
            throw new ATMNotFoundException();
        }
        return $atm;
    }

    /**
     * @param ATMDTO $dto
     * @return ATM
     * @throws Throwable
     */
    public function store(ATMDTO $dto): ATM
    {
        $atm = new ATM();
        $atm->label = $dto->label;
        $atm->saveOrFail();
        return $atm;
    }

    /**
     * @param string $id
     * @param ATMDTO $dto
     * @return ATM
     * @throws ATMNotFoundException
     * @throws Throwable
     */
    public function update(string $id, ATMDTO $dto): ATM
    {
        $atm = $this->find($id);
        $atm->label = $dto->label;
        $atm->saveOrFail();
        return $atm;
    }

    /**
     * @param string $id
     * @throws ATMNotFoundException
     */
    public function delete(string $id): void
    {
        $atm = $this->find($id);
        $atm->delete();
    }
}
