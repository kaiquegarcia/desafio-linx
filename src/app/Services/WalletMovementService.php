<?php

namespace App\Services;

use App\Cache\WalletMovement;
use App\DTOs\WithdrawBankNoteDTO;
use App\Enums\OperationType;
use App\Exceptions\InsufficientBalanceException;
use App\Exceptions\InsufficientBankNotesException;
use App\Exceptions\UnavailableBankNotesException;
use App\Models\ATM;
use App\Models\ATMBankNote;
use App\Models\UserWallet;
use App\Repositories\WalletMovementRepositoryInterface;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class WalletMovementService
{
    public function __construct(
        private WalletMovementRepositoryInterface $repository
    ) { }

    protected function subtract(
        UserWallet $wallet,
        int $amount,
        OperationType $type
    ): void
    {
        $wallet->balance = floatval(bcsub("{$wallet->balance}", "$amount"));
        $wallet->saveOrFail();


        $this->repository->save(new WalletMovement(
            wallet_id: $wallet->id,
            operation_type: $type,
            amount: -$amount,
            balance: $wallet->balance
        ));
    }

    protected function add(
        UserWallet $wallet,
        int $amount,
        OperationType $type
    ): void
    {
        $wallet->balance = floatval(bcadd("{$wallet->balance}", "$amount"));
        $wallet->saveOrFail();

        $this->repository->save(new WalletMovement(
            wallet_id: $wallet->id,
            operation_type: $type,
            amount: $amount,
            balance: $wallet->balance
        ));
    }

    public function transfer(
        string $fromWalletId,
        string $toWalletId,
        float $amount
    ): void
    {
        // TODO
    }

    public function openBalance(
        string $walletId,
        float $balance
    ): void
    {
        $movement = new WalletMovement(
            wallet_id: $walletId,
            operation_type: OperationType::OPENING_BALANCE(),
            amount: $balance,
            balance: $balance
        );
        $this->repository->save($movement);
    }

    public function reBalance(
        string $walletId,
        float $previousBalance,
        float $newBalance
    ): void
    {
        $change = floatval(bcsub("$newBalance", "$previousBalance"));
        $movement = new WalletMovement(
            wallet_id: $walletId,
            operation_type: OperationType::BALANCE_MAINTENANCE(),
            amount: $change,
            balance: $newBalance
        );
        $this->repository->save($movement);
    }

    public function all(UserWallet $wallet): Collection
    {
        return $this->repository->all($wallet->id);
    }
}
