<?php

namespace App\Services;

use App\Enums\OperationType;
use App\Models\UserWallet;

class DepositService extends WalletMovementService
{
    public function deposit(
        UserWallet $wallet,
        int $amount
    ): void
    {
        $this->add($wallet, $amount, OperationType::DEPOSIT());
    }
}
