<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self SAVINGS()
 * @method static self CHECKING()
 */
final class WalletType extends Enum
{
}
