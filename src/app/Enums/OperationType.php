<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self WITHDRAW()
 * @method static self DEPOSIT()
 * @method static self TRANSFER()
 * @method static self OPENING_BALANCE()
 * @method static self BALANCE_MAINTENANCE()
 */
final class OperationType extends Enum
{
}
