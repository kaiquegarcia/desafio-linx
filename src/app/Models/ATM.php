<?php

namespace App\Models;

use App\Traits\WithIdAsUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ATM extends Model
{
    use SoftDeletes, WithIdAsUuid, HasFactory;

    protected $fillable = [
        'label',
    ];

    protected $keyType = 'string';
    protected $table = 'atms';
    public $incrementing = false;

    public function bank_notes(): HasMany
    {
        return $this->hasMany(ATMBankNote::class, 'atm_id');
    }
}
