<?php

namespace App\Models;

use App\Enums\WalletType;
use App\Traits\RegisterOpeningBalanceAfterCreated;
use App\Traits\WithIdAsUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWallet extends Model
{
    use SoftDeletes,
        WithIdAsUuid,
        RegisterOpeningBalanceAfterCreated,
        HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'label',
        'balance',
    ];

    protected $keyType = 'string';
    public $incrementing = false;

    protected $casts = [
        'type' => WalletType::class,
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
