<?php

namespace App\Models;

use App\Traits\WithIdAsUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ATMBankNote extends Model
{
    use SoftDeletes, WithIdAsUuid, HasFactory;

    protected $fillable = [
        'atm_id',
        'bank_note',
        'amount',
    ];

    protected $keyType = 'string';
    protected $table = 'atm_bank_notes';
    public $incrementing = false;

    public function atm(): BelongsTo
    {
        return $this->belongsTo(ATM::class, 'atm_id');
    }
}
