<?php

namespace App\Models;

use App\Traits\WithIdAsUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes, WithIdAsUuid, HasFactory;

    protected $fillable = [
        'name',
        'birth_date',
        'cpf',
    ];

    protected $keyType = 'string';
    public $incrementing = false;

    public function wallets(): HasMany
    {
        return $this->hasMany(UserWallet::class);
    }
}
