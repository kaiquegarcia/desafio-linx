<?php

namespace App\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class ATMDTO extends DataTransferObject
{
    public string $label;
}
