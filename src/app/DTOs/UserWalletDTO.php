<?php

namespace App\DTOs;

use App\Enums\WalletType;
use Spatie\DataTransferObject\DataTransferObject;

class UserWalletDTO extends DataTransferObject
{
    public WalletType $type;
    public string $label;
    public ?float $balance;
}
