<?php

namespace App\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class WithdrawBankNoteDTO extends DataTransferObject
{
    public string $id;
    public string $atm_id;
    public int $bank_note;
    public int $amount;
}
