<?php

namespace App\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class UserDTO extends DataTransferObject
{
    public string $name;
    public string $birth_date;
    public string $cpf;
}
