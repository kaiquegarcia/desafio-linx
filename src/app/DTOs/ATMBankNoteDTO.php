<?php

namespace App\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class ATMBankNoteDTO extends DataTransferObject
{
    public ?int $bank_note;
    public int $amount;
}
