<?php

namespace App\Cache;

use App\Enums\OperationType;
use App\Models\ATM;
use App\Models\UserWallet;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;
use stdClass;

class WalletMovement implements JsonSerializable
{

    public function __construct(
        private ?string $id = null,
        private ?string $wallet_id = null,
        private ?string $atm_id = null,
        private ?OperationType $operation_type = null,
        private ?int $amount = null,
        private ?float $balance = null,
        private ?string $created_at = null
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWalletId(): ?string
    {
        return $this->wallet_id;
    }

    public function getAtmId(): ?string
    {
        return $this->atm_id;
    }

    public function getOperationType(): ?OperationType
    {
        return $this->operation_type;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function prePersist(): void
    {
        $this->id = Str::uuid();
        $this->created_at = Carbon::now()->format(DATE_ISO8601);
    }

    public function wallet(): Model|Collection|Builder|array|null
    {
        if (!$this->wallet_id) {
            return null;
        }
        return UserWallet::query()->find($this->wallet_id);
    }

    public function atm(): Model|Collection|Builder|array|null
    {
        if (!$this->atm_id) {
            return null;
        }
        return ATM::query()->find($this->atm_id);
    }

    #[ArrayShape([
        'id' => "null|string",
        'wallet_id' => "null|string",
        'atm_id' => "null|string",
        'operation_type' => "null|string",
        'amount' => "int|null",
        'balance' => "float|null",
        'created_at' => "null|string",
    ])]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'wallet_id' => $this->wallet_id,
            'atm_id' => $this->atm_id,
            'operation_type' => $this->operation_type?->value,
            'amount' => $this->amount,
            'balance' => $this->balance,
            'created_at' => $this->created_at,
        ];
    }

    public static function jsonUnserialize(array|stdClass|string $json): static
    {
        if (is_string($json)) {
            $json = json_decode($json);
        }
        if (is_array($json)) {
            $json = (object) $json;
        }
        return new static(
            id: $json?->id,
            wallet_id: $json?->wallet_id,
            atm_id: $json?->atm_id,
            operation_type: OperationType::tryFrom($json?->operation_type),
            amount: $json?->amount,
            balance: $json?->balance ?? null,
            created_at: $json?->created_at
        );
    }
}
