## Sobre a Cache

Utilizei o Redis por sua capacidade de resposta rápida perante a combinação `chave-valor`.

A estrutura que decidi usar para armazenar o estrato foi a seguinte:

- chaves: `wallet_{id da carteira}_movements`;
- valores:
```json
{
  "id": "uuid para a transação",
  "wallet_id": "uuid da carteira onde a movimentação aconteceu",
  "atm_id": "uuid do caixa eletrônico, se houver, quando relacionado a saques",
  "operation_type": "string, controlada pelo enum OperationType, representando o tipo de operação",
  "amount": "valor da transação, sendo positivo para entradas e negativo para saídas",
  "balance": "saldo após a transação",
  "created_at": "data/hora da transação"
}
```

Com isso, podemos criar estruturas de impressão de extrato no front-end ou utilizar essa cache para re-calcular o saldo da carteira, servindo como uma base segura sobre os valores transacionados.

Por exemplo: poderíamos criar um cronjob para rodar às 00:00, onde iria pegar todas as transações do dia anterior e validar se o saldo registrado no banco está correto baseado apenas nas entradas e saídas que ocorreram no dia.

Obviamente, essa periodicidade poderia ser ainda menor para evitar que um saldo fique com valor errado por muito tempo.

Sobre as demais formas de armazenamento, você pode encontrar detalhes na [Documentação sobre o Banco de Dados](../database/README.md)
