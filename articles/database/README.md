## Sobre o banco de dados

## Proposta inicial
![Entity-Relation Diagram](erd.png)

Refleti um pouco sobre as necessidades do projeto e concluí que algumas entidades são necessárias para seu desenvolvimento. São elas:

- User: representando os usuários da aplicação, independente do nível de acesso (admin / consumer);
- Wallet: representando as carteiras (ou contas) de cada usuário, não havendo uma limitação de quantas carteiras poderiam ser registradas em um;
- WalletMovement: representando toda e qualquer movimentação financeira, incluindo o saldo inicial da carteira, para garantir operações não-bloqueantes;
- ATM: representando os caixas eletrônicos disponíveis, ignorando outras lógicas da realidade (dependência de um cartão como autorizador junto de uma senha, etc);
- ATMBankNote: representando as cédulas disponíveis nos caixas eletrônicos, onde recebem um identificador numérico (bank_note) e a quantidade de notas disponíveis (amount).

## Alterações

Entretanto, uma das exigências do desafio é que o extrato de movimentações financeiras do usuário deve ser armazenada em cache, o que nos impede de armazenar a entidade WalletMovements em banco.

Dito isso, alterei minha ideia inicial colocando uma coluna "balance" na entidade Wallet e migrando apenas os registros de transações para o Redis, deixando o banco da seguinte forma:

![Entity-Relation Diagram (forçado)](ERD_forced.png)

### Resumo sobre as estruturas:

- Tabela `users` (usuários):
```json
{
  "id": "chave primária uuid do usuário",
  "name": "nome do usuário",
  "birth_date": "data de nascimento do usuário",
  "cpf": "CPF do usuário",
  "created_at": "data/hora da criação do usuário",
  "updated_at": "data/hora de alteração do usuário",
  "deleted_at": "data/hora de exclusão do usuário, se excluído, ou null"
}
```
- Tabela `user_wallets` (carteiras de usuário):
```json
{
  "id": "chave primária uuid da carteira",
  "user_id": "chave estrangeira uuid indicando o usuário dono da carteira",
  "type": "tipo de carteira, controlado pelo Enum WalletType, sendo SAVINGS para poupança ou CHECKING para conta corrente",
  "label": "nome legível da carteira (um nome que o usuário dá ou servir para identificar o tipo da carteira)",
  "balance": "saldo da carteira",
  "created_at": "data/hora da criação da carteira",
  "updated_at": "data/hora de alteração da carteira",
  "deleted_at": "data/hora de exclusão da carteira, se excluída, ou null"
}
```
- Tabela `atms` (caixas eletrônicos):
```json
{
  "id": "chave primária uuid do caixa",
  "label": "nome legível do caixa eletrônico (um nome para identificá-lo)",
  "created_at": "data/hora da criação do caixa",
  "updated_at": "data/hora de alteração do caixa",
  "deleted_at": "data/hora de exclusão do caixa, se excluído, ou null"
}
```
- Tabela `atm_bank_notes` (cédulas de caixa eletrônico):
```json
{
  "id": "chave primária uuid da cédula",
  "atm_id": "chave estrangeira uuid indicando o caixa onde a cédula está",
  "bank_note": "valor da cédula (ex: se é R$20, então registramos 20)",
  "amount": "quantidade de cédulas disponíveis (mínimo é 0)",
  "created_at": "data/hora da criação do registro da cédula",
  "updated_at": "data/hora de alteração do registro da cédula",
  "deleted_at": "data/hora de exclusão do registro da cédula, se excluído, ou null"
}
```

---

Sobre a estrutura de dados nas movimentações, deixarei documentado no [README sobre Cache](../cache/README.md)
