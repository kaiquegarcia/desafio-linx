Documentação da criação do projeto
---

1. [Sobre o banco de dados](database/README.md)
2. [Sobre a cache](cache/README.md)

---
Definições técnicas
---

- **WebServer**: NGINX
- **Gerenciador de processos**: PHP-FPM
- **Linguagem**: PHP 8.0
- **Framework**: Lumen 8
- **Banco de dados**: MySQL
- **Cache**: Redis

![Comunicação entre tecnologias](./stack.jpeg)

Todas as imagens do Docker foram minuciosamente selecionadas no Docker Hub.
