FROM php:8.0-rc-fpm

# Pré-requisitos
RUN apt-get update && apt-get -y install libzip-dev libonig-dev zip curl wget build-essential software-properties-common
RUN docker-php-ext-install zip mbstring bcmath

# MySQL
RUN docker-php-ext-install mysqli pdo pdo_mysql \
    && docker-php-ext-enable pdo_mysql

WORKDIR /app

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
COPY ./src/composer.* ./
RUN composer install --prefer-dist --no-autoloader \
    && composer dump-autoload;

# Sincronizar código-fonte
COPY ./src/ /app

# Atualizar permissões
RUN mkdir -p /app/storage/logs \
    && rm -f /app/storage/logs/app.log \
    && chmod -R 755 /app/storage/ \
    && chown -R www-data:www-data /app/storage

RUN touch /app/storage/logs/app.log && chown -R www-data: /app/storage
