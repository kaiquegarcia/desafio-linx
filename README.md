# Desafio Backend Linx Digital

### Teste Simulador Caixa Eletrônico

**Candidato**: Kaique Garcia Menzes ([contato@kaiquegarcia.dev](mailto:contato@kaiquegarcia.dev))

Instalação
---

1. Crie a rede interna dos containers: `docker network create bubble`;
2. Instale os containers do Docker rodando no terminal posicionado no diretório do projeto com o
   comando `docker compose up -d`;
3. Verifique se a pasta `vendor` foi criada dentro da pasta `src`. Caso negativo, execute o composer: `docker exec -it app.dev composer install --prefer-dist`;
4. Execute as migrations: `docker exec -it app.dev php artisan migrate`;
5. Opcionalmente, execute os seeders para alimentar o banco de dados: `docker exec -it app.dev php artisan db:seed`;
6. Dando tudo certo, a API ficará disponível no endereço `http://localhost:8080/api`.

---
Sobre o desenvolvimento
---

Minha intenção inicial era desenvolver o código estruturando em DDD (Domain-Driven) seguindo TDD (Test-Driven) e preenchendo uma documentação Swagger via lib `swagger-lume`, mas acabei tendo alguns contratempos pessoais e não consegui desenvolver da forma que tinha planejado inicialmente.

Acabei desfazendo algumas lógicas no meio do caminho. Por exemplo, pretendia criar estrutura de `Repository` tanto para o Eloquent quanto para o Redis, mas manti somente o de Redis. Além disso, o DDD consiste em separar precisamente o que é domínio da aplicação e o que é recurso externo, seja infraestrutura ou literalmente outros microsserviços, o que requer um estudo mais preciso sobre o produto, o que tomaria mais tempo ainda.

Além disso, ao invés de documentar tudo em Swagger, eu consegui montar rapidinho uma collection do Postman, o que acredito que sirva para rodar localmente.

Outro problema que tive ao longo do desenvolvimento, é que minha máquina pessoa está com Windows 10 (Insider) e o WSL (Windows Subsystem Linux) não está deixando o Docker linkar os diretórios na instalação. Fora do WSL, o PC chegou a dar a famosa tela azul (que agora é verde, rs). Então passei alguns problemas com limitação de memória para testar.

Fora isso o que foi mencionado, acredito que deixei o projeto suficientemente abstraído para evoluções, sendo, inicialmente, um monolito, se o projeto tomasse outras proporções poderia naturalmente criar microsserviços para cada operação. Outro fator importante que preciso mencionar, é que o banco relacional das carteiras tem um campo `balance` que representa o saldo da carteira. Essa **não é uma boa prática**, mas fiz dessa forma porque entendi que era desejado manter o extrato somente em cache. Por experiência, eu diria que essa deveria ser a primeira correção de débito técnico do projeto MVP, pois bloqueia diretamente a capacidade de escalonamento da aplicação.

O motivo é simples: por se tratar de um dado persistido no banco, toda operação poderia resultar em um lock na tabela, causando um problema gigante ao ter muitas transações por segundo. As requisições precisariam aguardar cada lock até chegar sua vez de ser processada no banco.

Sem esse lock, o problema que ocorreria seria ainda pior: duas requisições de transação na mesma carteira poderiam criar o pior caso de uma não considerar a transação da outra.

Por exemplo, se por algum milagre eu conseguisse executar o comando de saque em 2 caixas eletrônicos diferentes ao mesmo tempo sem o lock, um sacando R$50 e outro R$20, as transações poderiam se ignorar... E o resultado final no saldo poderia ser -R$50 ou -R$20, ao invés de -R$70.

Sendo assim, ressalto nessa documentação a minha percepção como preocupação sobre a estrutura de dados sugerida.

Sobre a cache, utilizei o Redis como forma de armazenamento. Ainda assim, organizei o código com interfaces de Repository apartada das lógicas do Redis, de forma que poderíamos facilmente mudar a infraestrutura apenas criando outro repository da nova tecnologia e mudando qual classe está implementando a interface no Lumen.

Sobre o banco de dados, utilizei MySQL mas utilizei o próprio ORM do Eloquent ao invés de criar a lógica de Repositories. Acredito que essa deveria ser o segundo débito técnico a ser desenvolvido, caso haja algum risco de troca de banco de dados ao longo da aplicação.

Enfim, aqui encerro o meu descritivo sobre como foi o desenvolvimento dessa aplicação.

---
Instruções de uso manual
---

1. Baixe o [Postman Collection da API](articles/collection.json) e abra no [Postman](https://www.postman.com/downloads/);
2. Configure a Collection, clicando no nome dela após importá-la e indo na guia "Variables". Lá terá algumas variáveis críticas da aplicação nas outras rotas;
3. Na collection, há duas pastas dentro da pasta `app.dev`:
   1. `administração`: aqui está presente todas as rotas necessárias para configurar as entidades do projeto - você vai precisar ao menos consultar os usuários, carteiras e caixas eletrônicos pro aqui para testar a aplicação. Se não rodou os Seeders, precisará usar as rotas de cadastro para inserir os registros;
   2. `rotas de usuário`: aqui está presente todas as rotas de uso do produto, sendo consulta de carteiras, extrato e ação de depositar ou sacar dinheiro.

Obs.: As rotas de usuário tem uma falsa autenticação, que é colocar o campo `USER_ID` nos headers, inserido o id do usuário que está processando as chamadas. A ideia aqui era de que o serviço de federação seria apartado da aplicação e receberíamos apenas a identidade dele, sem precisar autenticá-lo.

---
Instruções de testes automatizados com PHPUnit
---

Conforme mencionei na descrição da API, eu não consegui desenvolver testes automatizados o suficiente por dificuldades pessoais. Ainda assim, deixarei abaixo as instruções que eu tinha escrito antes de começar a desenvolver:

1. Você pode rodar todos os testes registrados com o seguinte
   comando: `docker exec -it app.dev php ./vendor/bin/phpunit`;
2. Você também pode rodar testes filtrando pelo nome da classe ou nome do método que deseja averiguar, seja para testar
   valores diferentes dos planejados ou só checar seus resultados, através do
   comando `docker exec -it app.dev php ./vendor/bin/phpunit --filter={nome do que quer filtrar}`, substituindo o
   trecho `{nome do que quer filtrar}` pela busca desejada.

---
Mais detalhes
---

Separei informações adicionais sobre o projeto (por exemplo, o que me guiou a tomar algumas decisões) em outros READMEs. [Você pode acessar o índice aqui](articles/README.md).
